#!/usr/bin/env bash

GB3DIR="$(which gbr3 2>/dev/null)"

InstallName="${GB3DIR%/*}/groot"
echo "Press Return to Install as '$InstallName'"
read -e -p "Ctrl-C to cancel: "

sudo cp -v ./groot.gambas "$InstallName"
if [ $? -ne 0 ]; then
echo "An error occured !"
else
echo "No errors"
fi

read -e -p "All Done."

